Cholera MUD

Mini-MUD, based on Vedmak MUD code https://github.com/tmud/vedmak
with smallest world and empty data files

Language: Russian
Theme: Vedmak (The Witcher or Wiedźmin)

Compiling in Linux

make linux

Compiling in Windows (cygwin)

make cygwin

Binaries for Windows: http://files.mud.kharkov.org/cholera-mud.zip

Prool

jan'2023, war time
Kharkiv, Ukraine
