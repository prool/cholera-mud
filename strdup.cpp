// prool: strdup for cygwin

#include <stdlib.h>
#include <string.h>

char * strdup (const char *s)
{
void *n;

  size_t len = strlen (s) + 1;
  n = malloc (len);
  if (n == NULL)
    return NULL;
  return (char *) memcpy (n, s, len);
}
