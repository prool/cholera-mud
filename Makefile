# Makefile

CC = gcc

RESFILE = cmud

LIBS = -lstdc++

INCLUDES = -I ./src

SOURCES = src/*.cpp
 
linux:	
	$(CC) -ggdb3 -m32 -std=c++98 -pedantic -o $(RESFILE) $(SOURCES) $(INCLUDES) -D NDEBUG $(LIBS) > err.log 2>&1

cygwin:	
	$(CC) -fno-permissive -ggdb3 -std=c++98 -pedantic -o $(RESFILE) $(SOURCES) strdup.cpp $(INCLUDES) -DNDEBUG -DCYGWIN $(LIBS) > err.log 2>&1
